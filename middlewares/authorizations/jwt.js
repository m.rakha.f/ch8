const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET;

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function middleware(req, res, next) {
  // object has own property
  if(!req.headers || !req.headers.hasOwnProperty('token')) {
    return next();
  }

  jwt.verify(req.headers.token, jwtSecret, (err, result) => {
    if(!err && result.logged_in) {
      req.authorized = true;
      req.is_admin = result.is_admin;
      req.user_id = result.user_id;
    }

    next();
  });
}

module.exports = middleware;
