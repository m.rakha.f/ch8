/** @type {import('../../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.authorized) {
    return res.status(401).json({
      error: true,
      message: 'Unauthorized',
      data: [],
    });
  }

  if(
    !req.body.user_id
    || !req.body.game
    || !req.body.score
  ){
    return res.json({
      error: true,
      message: 'User id, game and score are required',
      data: [],
    })
  }

  var id = parseInt(req.params.id);

  if(isNaN(id)) {
    return res.json({
      error: true,
      message: 'Invalid user history id',
      data: [],
    })
  }

  const getData = await prisma.user_game_history.findFirst({
    where: {
      id
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(getData && getData.error){
    return res.status(500).json(getData)
  }

  if(
    getData != null
    && getData.user_id != req.user_id
    && !req.is_admin
  ) {
    return res.status(401).json({
      error: true,
      message: 'Unauthorized',
      data: [],
    });
  }

  const update = await prisma.user_game_history.update({
    where: {
      id
    },
    data: {
      user_game: {
        connect: {
          id: parseInt(req.body.user_id)
        }
      },
      game: req.body.game,
      score: parseInt(req.body.score)
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(update && update.error){
    return res.status(500).json(update)
  }

  res.json({
    error: false,
    message: 'User history updated',
    data: [update],
  })
}

module.exports = controller;
