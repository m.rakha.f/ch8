const multer = require("multer");
const uploader = multer(
  {
    dest: process.env.ROOT_PATH + '/storage/',
    limits: {
      fileSize: 1000000,
    },
  }
).single('video');
/** @type {import('../../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(
    !req.authorized
    || (req.body.user_id != req.user_id && !req.is_admin)
  ) {
    return res.status(401).json({
      error: true,
      message: 'Unauthorized',
      data: [],
    });
  }

  uploader(req, res, async (err) => {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      return res.status(500).json({
        error: true,
        message: err.message,
        data: [],
      });
    }

    if (err) {
      // An unknown error occurred when uploading.
      return res.status(500).json({
        error: true,
        message: err.toString(),
        data: [],
      });
    }

    const data = await prisma.video.create({
      data: {
        filename: req.file.filename,
        size: req.file.size,
        mimetype: req.file.mimetype,
        user_game: {
          connect: {
            id: parseInt(req.body.user_id)
          }
        },
      }
    }).catch(err => {
      return {
        error: true,
        message: err.message,
        data: [],
      }
    });

    if(data && data.error){
      return res.status(500).json(data)
    }

    // Everything went fine.
    return res.json({
      error: false,
      message: 'Upload success',
      data: [data],
    });
  })
}

module.exports = controller;
