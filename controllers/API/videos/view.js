/** @type {import('../../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.authorized) {
    return res.status(401).json({
      error: true,
      message: 'Unauthorized',
      data: [],
    });
  }

  const data = await prisma.video.findFirst({
    where: {
      id: req.params.id,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(data && data.error) {
    return res.status(500).json(data);
  }

  if(
    data != null
    && data.user_id != req.user_id
    && !req.is_admin
  ) {
    return res.status(401).json({
      error: true,
      message: 'Unauthorized',
      data: [],
    });
  }

  res.json({
    error: false,
    message: 'Video found',
    data: [data],
  });
}

module.exports = controller;
