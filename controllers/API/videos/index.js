module.exports = {
  all: require('./all'),
  store: require('./store'),
  view: require('./view'),
}
